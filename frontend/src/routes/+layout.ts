export const ssr = true; // For SEO, fully render pages when building them.
export const csr = true; // Hydrate pages
export const prerender = true;
