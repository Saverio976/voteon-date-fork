/** Turns a "string" into a "String", capitalizing the first character. */
function capitalize(s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
}

/** Returns an array of numbers, starting from `start` and ending with (but not including) `end`. */
function range(start: number, end: number): number[] {
    const rangeArray = <number[]>[];
    for (let i = start; i <= end; i++) {
        rangeArray.push(i);
    }
    return rangeArray;
}

/** Turns a number into a string and mkes sure the result has at least two digits. */
function addZero(nr: number): string {
    const formattedTime: string = nr < 10 ? `0${nr}` : nr.toString();
    return formattedTime;
}

/** Combines the dayte's year, month, and day with the provided hour and minute. */
function combineDayHourMinute(dayte: Date, hour: number, minute: number): Date {
    return new Date(
        dayte.getFullYear(),
        dayte.getMonth(),
        dayte.getDate(),
        hour,
        minute
    );
}

function copyTextToClipboard(text: string) {
    // Copy the URL in the input-field "linkP" to the clipboard.

    navigator.clipboard.writeText(text).then(
        function () {
            console.info("Async: Copying to clipboard was successful!");
        },
        function (err) {
            console.error("Async: Could not copy text: ", err);
        }
    );
}

/** Returns the min and max values of an array of values. */
function minMax<T>(items: T[]): [T, T] {
    return items.reduce(
        (acc, value) => {
            acc[0] = value < acc[0] ? value : acc[0]; // min
            acc[1] = value > acc[1] ? value : acc[1]; // max
            return acc;
        },
        [items[0], items[0]]
    );
}

/** Syncs the vertical scrolling of the two scroll-views. */
function syncScrolling() {
    const timeScrollView = document.querySelector("#time-scroll-view"),
        headersScrollView = document.querySelector("#headers-scroll-view");
    if (timeScrollView !== null && headersScrollView !== null) {
        headersScrollView.scrollLeft = timeScrollView.scrollLeft;
    }
}

/** Adds shadows to the calendar whenever appropriate. */
/* function updateShadows() {
    const timeScrollView = document.querySelector("#time-scroll-view");
    if (timeScrollView !== null) {
        const t = <HTMLElement>timeScrollView,
            displayShadowTop = t.scrollTop !== 0,
            displayShadowRight =
                t.scrollWidth - t.scrollLeft - t.clientWidth !== 0,
            displayShadowBottom =
                t.scrollHeight - t.scrollTop - t.clientHeight !== 0,
            displayShadowLeft = t.scrollLeft !== 0;

        if (
            displayShadowTop ||
            displayShadowRight ||
            displayShadowBottom ||
            displayShadowLeft
        ) {
            t.style.background = `\
${displayShadowTop ? "var(--shadow-top)" : ""}\
${
    displayShadowTop &&
    (displayShadowRight || displayShadowBottom || displayShadowLeft)
        ? ", "
        : ""
}\
${displayShadowRight ? "var(--shadow-right)" : ""}\
${displayShadowRight && (displayShadowBottom || displayShadowLeft) ? ", " : ""}\
${displayShadowBottom ? "var(--shadow-bottom)" : ""}\
${displayShadowBottom && displayShadowLeft ? ", " : ""}\
${displayShadowLeft ? "var(--shadow-left)" : ""}`;
        }

        /* const headersScrollView = document.querySelector("#headers-scroll-view");
        if (headersScrollView !== null) {
            const h = <HTMLElement>headersScrollView;
            h.style.background = `\
${displayShadowRight ? "var(--shadow-right)" : ""}\
${displayShadowRight && displayShadowLeft ? ", " : ""}\
${displayShadowLeft ? "var(--shadow-left)" : ""}`;
        } * /
    }
} */

/**
 * Sends a `GET`-request to a given `path`-string.
 * Returns a `Promise<T>` of the data contained in the body.
 */
async function fetchData<T>(path: string): Promise<T> {
    try {
        const response = await fetch(path);

        if (response.ok) {
            const poll = response.json();
            return Promise.resolve<T>(poll);
        } else {
            return Promise.reject<T>(`Path "${path}" resulted in error.`);
        }
    } catch (e) {
        return Promise.reject<T>(e);
    }
}
/**
 * Sends a PATCH request to the backend, setting a vote's
 * "the admin has edited something"-flag to `false`.
 */
async function sendUnsetAdminEditedRequest(pollId: string, voteId: string) {
    console.info("Starting unset_admin_edited-request");
    const response = await fetch(`/${pollId}/unset_admin_edited/${voteId}`, {
        method: "PATCH",
    });
    if (response.ok) {
        console.info("Request was successful.");
    } else {
        console.error(
            `PATCH /${pollId}/unset_admin_edited/${voteId} returned an error.`
        );
    }
}

const dialogStorageSuffix = "-dialog";
/**
 * Writes the dialog's title into SessionStorage.
 * Only run this function when the window-object is accessible.
 */
function saveDialogText(pollId: string, content: string) {
    localStorage.setItem(`${pollId}${dialogStorageSuffix}`, content);
}

/**
 * Returns the text stored under the key "dialog", if it exists, and then deletes it.
 * Only run this function when the window-object is accessible.
 */
function loadDialogText(pollId: string): string | null {
    const content = localStorage.getItem(`${pollId}${dialogStorageSuffix}`);
    localStorage.removeItem(`${pollId}${dialogStorageSuffix}`);
    return content;
}

/**
 * The searchParameter name used in URLs to specify what URL to return to instead of the
 * usual previous page.
 */
const returnToParamName = "f";
/** Adds the provided returnToUrltr to the end of the provided URL, as a parameter. */
function addReturnToParam(urlStr: string, returnToUrlStr: string): string {
    const encodedReturnToURI = encodeURI(returnToUrlStr);
    return `${urlStr}?${returnToParamName}=${encodedReturnToURI}`;
}
/** Checks whether it is probably the poll's admin who is visiting this page. */
function inAdminMode(currentUrl: URL): boolean {
    let returnToParam = null;
    try {
        returnToParam = currentUrl.searchParams.get(returnToParamName);
    } catch (error) {
        console.error(error);
    }
    if (returnToParam !== null) {
        const returnToUrl = new URL(returnToParam);
        if (currentUrl.hostname === returnToUrl.hostname) {
            return true;
        }
    }
    return false;
}
/**
 * If the current URL contains the return-To-Paramater with an acceptable URL,
 * that URL will get selected.
 * Otherwise, the desiredNextLocation will be used.
 */
function chooseURL(currentUrl: URL, desiredNextLocation: string): string {
    let returnToParam = null;
    try {
        returnToParam = currentUrl.searchParams.get(returnToParamName);
    } catch (error) {
        console.error(error);
    }
    if (returnToParam !== null) {
        const returnToUrl = new URL(returnToParam);
        if (currentUrl.hostname === returnToUrl.hostname) {
            return returnToUrl.href;
        }
    }
    return desiredNextLocation;
}

export {
    capitalize,
    range,
    addZero,
    combineDayHourMinute,
    copyTextToClipboard,
    minMax,
    syncScrolling,
    //updateShadows,
    fetchData,
    sendUnsetAdminEditedRequest,
    saveDialogText,
    loadDialogText,
    addReturnToParam,
    inAdminMode,
    chooseURL,
};
