-- This file should undo anything in `up.sql`
-- Your SQL goes here

ALTER TABLE polls ALTER COLUMN offered_datetimes TYPE TIMESTAMPTZ(0)[100];
ALTER TABLE votes ALTER COLUMN availability TYPE TIMESTAMPTZ(0)[250];