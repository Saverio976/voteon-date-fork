//! A collection of functions that handle web-requests.

mod dev_api;
pub(crate) mod file_server_api;
pub(crate) mod open_api;
mod poll_actions;
mod poll_data;
mod poll_sites;

use dev_api::*;
use file_server_api::*;
pub use poll_actions::*;
pub use poll_data::*;
use poll_sites::*;

use rocket::{
    http::{ext::IntoOwned, Method, Status},
    response::Redirect,
    route::{Handler, Outcome},
    Data, Request, Route,
};
use rocket_okapi::openapi_get_routes;

/// A handler that redirects to the no-trailing-slash version
/// of the URL, if the URL ends in a trailing slash.
#[derive(Clone)]
pub struct SlashRemover;
#[rocket::async_trait]
impl Handler for SlashRemover {
    async fn handle<'r>(&self, req: &'r Request<'_>, data: Data<'r>) -> Outcome<'r> {
        let uri = req.uri();

        if uri.path().ends_with('/') && uri.path().to_string().chars().count() > 1 {
            // = if uri ends with "/" and doesn't point to root:
            Outcome::from(
                req,
                Redirect::permanent(uri.clone().into_owned().into_normalized()),
            )
        } else {
            // Use next handler. Not 100% sure if `Status::Ok` is the most appropriate status-code.
            Outcome::forward(data, Status::Ok)
        }
    }
}
impl From<SlashRemover> for Vec<Route> {
    fn from(sr: SlashRemover) -> Vec<Route> {
        vec![Route::new(Method::Get, "/<..>", sr)]
    }
}

/// The [`rocket::Route`]s used in release builds.
/// **Make sure these dont include any routes from [dev_api]!**
pub fn release_routes() -> Vec<rocket::Route> {
    routes![
        // File-server-APIs
        root_index,
        file_server,
        root_add_html,
        //
        // Get data
        get_poll_title,
        get_poll_setupdata,
        get_poll_data,
        get_vote_data,
        get_results_data,
        // Manipulate data
        create_poll,
        edit_poll,
        delete_poll,
        add_vote,
        update_vote,
        unset_admin_edited,
        delete_vote,
        // Pages
        admin_links_page,
        edit_poll_page,
        results_page,
        vote,
        vote_links_page,
        update_vote_page,
    ]
}

/// The [`rocket::Route`]s included in debug builds.
/// These include routes from [dev_api], which help to get an overview of the database's state.!
pub fn dev_routes() -> Vec<rocket::Route> {
    openapi_get_routes![
        //
        // Dev-APIs
        test,
        error,
        get_db,
        //
        // The remaining routes should be the same as provided in [release_routes]:
        //
        // File-server-APIs
        root_index,
        file_server,
        root_add_html,
        //
        // Get data
        get_poll_title,
        get_poll_setupdata,
        get_poll_data,
        get_vote_data,
        get_results_data,
        // Manipulate data
        create_poll,
        edit_poll,
        delete_poll,
        add_vote,
        update_vote,
        unset_admin_edited,
        delete_vote,
        // Pages
        admin_links_page,
        edit_poll_page,
        results_page,
        vote,
        vote_links_page,
        update_vote_page,
    ]
}
