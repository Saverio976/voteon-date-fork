//! The part of the API which serves static files, according to the provided path.

use crate::errors::ApiResult;

use rocket::{
    fs::NamedFile,
    get,
    http::{
        uri::{fmt::Path as RocketPath, Segments},
        Status,
    },
    request::{FromParam, FromRequest, FromSegments, Outcome},
    response::{self, Responder, Response},
    Request,
};
use rocket_okapi::{
    gen::OpenApiGenerator,
    okapi::openapi3::Responses,
    openapi,
    request::{OpenApiFromRequest, RequestHeaderInput},
    response::OpenApiResponderInner,
    OpenApiError,
};
use schemars::JsonSchema;
use std::{
    ffi::OsStr,
    path::{Path, PathBuf},
};

pub const ROOT_ROUTE: &str = "../frontend/build";
pub const ROOT_PAGES: [&str; 5] = ["index", "create", "about", "privacy", "thanks"];

/// Struct used to check whether the requested file has a file-extension.
/// Succeedes if there IS a file-extension (= a period).
#[derive(JsonSchema)]
pub struct PathWithFileExtension(PathBuf);
impl<'r> FromSegments<'r> for PathWithFileExtension {
    type Error = &'r str;

    fn from_segments(segments: Segments<'r, RocketPath>) -> Result<Self, Self::Error> {
        let path = PathBuf::from_segments(segments).map_err(|_| "Not a path.")?;

        match path.extension() {
            Some(_) => Ok(PathWithFileExtension(path)),
            None => Err("No file extension."),
        }
    }
}

/// Struct used to check whether the requested file has a file-extension.
/// Succeedes if there is NO file-extension and if the desired page actually
/// exists in root.
#[derive(JsonSchema)]
pub struct TruncatedRootHtml(PathBuf);
#[rocket::async_trait]
impl<'r> FromParam<'r> for TruncatedRootHtml {
    type Error = &'r str;

    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        let path = PathBuf::from_param(param).map_err(|_| "Not a path.")?;

        if path.extension().is_some() {
            return Err("File has a file-extension.");
        }

        match ROOT_PAGES.contains(&param) {
            true => Ok(TruncatedRootHtml(path)),
            false => Err("Not defined as a root-level html-file"),
        }
    }
}

/// An enum representing the compression algorithms that might be used for delivered files.
/// These are limited to what [Svelte's adapter-static](https://github.com/sveltejs/kit/tree/master/packages/adapter-static) can produce.
#[derive(PartialEq, Eq, PartialOrd)]
pub enum CompressionAlgorithm {
    Brotli, // Preferred
    Gzip,   // Second most preferred
    UseNone,
}
impl CompressionAlgorithm {
    /// Creates [Self] out of one of [these](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Encoding#directives).
    /// The [`value`] may contain additional characters.
    pub fn from_accept_encoding_value(value: &str) -> Self {
        if value.contains("br") {
            CompressionAlgorithm::Brotli
        } else if value.contains("gzip") {
            CompressionAlgorithm::Gzip
        } else {
            CompressionAlgorithm::UseNone
        }
    }
    /// Creates one of [these](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Encoding).
    pub fn as_header_value(&self) -> Option<&str> {
        match self {
            Self::Brotli => Some("br"),
            Self::Gzip => Some("gzip"),
            Self::UseNone => None,
        }
    }
    /// Returns the file-extension corresponding to [`self`].
    pub fn as_file_extension(&self) -> Option<&str> {
        match self {
            Self::Brotli => Some("br"),
            Self::Gzip => Some("gz"),
            Self::UseNone => None,
        }
    }
}

/// The list of acceptable [`CompressionAlgorithm`]s, ordered from most desired (index 0) to least desired (last index).
pub(crate) struct PreferredCompressions(Vec<CompressionAlgorithm>);
#[rocket::async_trait]
impl<'r> FromRequest<'r> for PreferredCompressions {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let accept_encoding_val_str: &str = match req
            .headers()
            .get("Accept-Encoding")
            .collect::<Vec<&str>>()
            .first()
        {
            Some(s) => s,
            None => {
                return Outcome::Success(PreferredCompressions(Vec::from([
                    CompressionAlgorithm::UseNone,
                ])));
            }
        };
        let mut compression_tuples = accept_encoding_val_str
            .split(' ')
            .filter_map(
                |encoding_option: &str| -> Option<(CompressionAlgorithm, f32)> {
                    // Look for compression-name
                    let compression =
                        CompressionAlgorithm::from_accept_encoding_value(encoding_option);
                    if compression == CompressionAlgorithm::UseNone {
                        return None;
                    }

                    // Figure out compression-weight
                    let weight = match encoding_option.split_once(";q=") {
                        Some((_, weight_str)) => match weight_str.parse::<f32>() {
                            Ok(w) => w,
                            Err(_err) => return None,
                        },
                        None => 1.0,
                    };
                    if weight == 0.0 {
                        return None;
                    }

                    Some((compression, weight))
                },
            )
            .collect::<Vec<(CompressionAlgorithm, f32)>>();

        // Sort the [CompressionAlgorithms] with the biggest weights towards the front.
        // If the weights are identical, use Brotli over Gzip
        compression_tuples.sort_by(|(c1, w1), (c2, w2)| {
            // At the moment this can only be Brotli or Gzip, which is why that first part of the [`if`]-statement isn't needed.
            if
            /* (c1 == &CompressionAlgorithm::Brotli && c2 == &CompressionAlgorithm::Gzip
            || c1 == &CompressionAlgorithm::Gzip && c2 == &CompressionAlgorithm::Brotli)
            && */
            (w1 - w2).abs() < 0.01 {
                c1.partial_cmp(c2).unwrap()
            } else {
                w2.partial_cmp(w1).unwrap()
            }
        });
        // Remove weights
        let mut preferred_compressions: Vec<CompressionAlgorithm> = compression_tuples
            .into_iter()
            .map(|(compression, _weight)| compression)
            .collect();

        // Always add UseNone as last option
        preferred_compressions.push(CompressionAlgorithm::UseNone);

        Outcome::Success(PreferredCompressions(preferred_compressions))
    }
}
impl<'r> OpenApiFromRequest<'r> for PreferredCompressions {
    /// Implementation required by OkApi. See [Here](https://github.com/GREsau/okapi#faq).
    fn from_request_input(
        _gen: &mut OpenApiGenerator,
        _name: String,
        _required: bool,
    ) -> rocket_okapi::Result<RequestHeaderInput> {
        Ok(RequestHeaderInput::None)
    }
}

/// A custom [`Responder`] that adds a [`content-encoding`-header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control)
/// where feasable to the response
pub struct EncodingResponder<T: OpenApiResponderInner> {
    data: T,
    compression: CompressionAlgorithm,
    /// Necessary to ensure the correct content-type is declared
    original_file_ext: Option<String>,
}
impl<'r, 'o: 'r, T: Responder<'r, 'o> + OpenApiResponderInner> Responder<'r, 'o>
    for EncodingResponder<T>
{
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'o> {
        let mut response = Response::build_from(self.data.respond_to(req)?);

        if let Some(compression_str) = self.compression.as_header_value() {
            // If there is a compressed file:
            let intermediate_response =
                response.raw_header("content-encoding", compression_str.to_string());

            if let Some(ext) = self.original_file_ext {
                return intermediate_response
                    .raw_header(
                        "content-type",
                        match ext.as_str() {
                            // These must be the same options declared in [`COMPRESSABLE_FILE_EXTENSIONS`]
                            "html" => "text/html; charset=utf-8",
                            "js" => "application/javascript",
                            "css" => "text/css; charset=utf-8",
                            "json" => "application/json",
                            "svg" => "image/svg+xml",
                            "xml" => "application/xml",
                            _ => return Err(Status::InternalServerError),
                        },
                    )
                    .ok();
            }
        }
        response.ok()
    }
}
impl<T: OpenApiResponderInner> OpenApiResponderInner for EncodingResponder<T> {
    fn responses(_generator: &mut OpenApiGenerator) -> Result<Responses, OpenApiError> {
        T::responses(_generator)
    }
}

/// A custom [`Responder`] that adds a [`Cache-Control`-header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control)
/// to the response
pub struct CacheResponder<T: OpenApiResponderInner> {
    pub data: T,
    /// Wether or not to send a caching-header
    pub use_caching: bool,
}
impl<'r, 'o: 'r, T: Responder<'r, 'o> + OpenApiResponderInner> Responder<'r, 'o>
    for CacheResponder<T>
{
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'o> {
        let cache_control_value = match self.use_caching {
            true => "public, max-age=31536000",
            false => "no-cache",
        };
        Response::build_from(self.data.respond_to(req)?)
            .raw_header("cache-control", cache_control_value)
            .ok()
    }
}
impl<T: OpenApiResponderInner> OpenApiResponderInner for CacheResponder<T> {
    fn responses(_generator: &mut OpenApiGenerator) -> Result<Responses, OpenApiError> {
        T::responses(_generator)
    }
}

/// File-extensions that may be compressed by [Svelte's adapter-static](https://github.com/sveltejs/kit/tree/master/packages/adapter-static)
const COMPRESSABLE_FILE_EXTENSIONS: [&str; 6] = ["html", "js", "css", "json", "svg", "xml"];
/// A function that tries to return compressed files where possible.
/// This is the only place where [`EncodingResponder`] is filled.
async fn get_compressed_file(
    pc: PreferredCompressions,
    path: PathBuf,
) -> ApiResult<EncodingResponder<NamedFile>> {
    if let Some(extension) = path.extension() {
        if let Some(ext_str) = extension.to_str() {
            if COMPRESSABLE_FILE_EXTENSIONS.contains(&ext_str) {
                for compression in pc.0 {
                    if compression == CompressionAlgorithm::UseNone {
                        continue;
                    }
                    let c_path = path.with_extension(format!(
                        "{}.{}",
                        ext_str,
                        compression.as_file_extension().unwrap()
                    ));
                    if let Ok(named_file) = NamedFile::open(c_path).await {
                        return Ok(EncodingResponder {
                            data: named_file,
                            compression,
                            original_file_ext: Some(ext_str.to_string()),
                        });
                    }
                }
            }
        }
    }

    // If any of the above checks fail, return an uncompressed file.
    Ok(EncodingResponder {
        data: NamedFile::open(path).await?,
        compression: CompressionAlgorithm::UseNone,
        original_file_ext: None,
    })
}

//
//
// The actual API
//
//

/// Forwards root (`/`) to `some_path/index.html`.
#[openapi(tag = "File Server")]
#[get("/")]
pub(crate) async fn root_index(
    pc: PreferredCompressions,
) -> ApiResult<EncodingResponder<NamedFile>> {
    let path = Path::new(ROOT_ROUTE).join("index.html");

    get_compressed_file(pc, path).await
}

/// The "normal" file-server.
/// Used for all those requests which try to GET files that contain a file-extension.
/// Example: `main.css`
#[openapi(tag = "File Server")]
#[get("/<path..>", rank = 1)]
pub(crate) async fn file_server(
    pc: PreferredCompressions,
    path: PathWithFileExtension,
) -> ApiResult<CacheResponder<EncodingResponder<NamedFile>>> {
    let path = Path::new(ROOT_ROUTE).join(path.0);

    let file_extension = path.extension();
    // Use caching for everything that isn't an `html`-file.
    let use_caching = match file_extension {
        None => false,
        Some(x) => x != OsStr::new("html"),
    };

    Ok(CacheResponder {
        data: get_compressed_file(pc, path).await?,
        use_caching,
    })
}

/// A file-server for requests, that try to get html-files from root without mentioning the `.html`-part.
/// It does this by appending ".html" to the provided path.
///
/// Used to allow for loading of `website/about`, which is prettier than `website/about.html`.
#[openapi(tag = "File Server")]
#[get("/<file_name>", rank = 2)]
pub(crate) async fn root_add_html(
    pc: PreferredCompressions,
    file_name: TruncatedRootHtml,
) -> ApiResult<EncodingResponder<NamedFile>> {
    let mut path = Path::new(ROOT_ROUTE).join(file_name.0);
    path.set_extension("html");

    get_compressed_file(pc, path).await
}
