//! Poll-specific requests that return a website.

use crate::errors::{ApiError, ApiResult};
use crate::models::db_models::Poll;
use crate::DbConn;

use rocket::{get, http::Status, response::status};
use rocket_dyn_templates::Template;
use rocket_okapi::openapi;

/// Uses the `validation_result` to decide on and return a page.
fn pick_response(
    validation_result: ApiResult<()>,
    success_path: &'static str,
    poll: &Poll,
) -> ApiResult<status::Custom<Template>> {
    match validation_result {
        Ok(()) => Ok(status::Custom(
            Status::Ok,
            Template::render(success_path, poll),
        )),
        Err(ApiError::IdValidation) => Ok(status::Custom(
            Status::Forbidden,
            Template::render("403/id-specific", poll),
        )),
        Err(e) => Err(e),
    }
}

// Admin-pages

/// Returns a page that displays the [`Poll`]'s access-links.
#[openapi(tag = "Admin pages")]
#[get("/<poll_id>/links/<admin_id>", rank = 3)]
pub(crate) async fn admin_links_page(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
) -> ApiResult<status::Custom<Template>> {
    let poll = conn.get_poll(poll_id.clone()).await?;

    pick_response(
        conn.validate_poll_admin(poll_id, admin_id).await,
        "links/admin-specific",
        &poll,
    )
}

/// Returns a page which allows for editing a [`Poll`].
#[openapi(tag = "Admin pages")]
#[get("/<poll_id>/edit/<admin_id>", rank = 3)]
pub(crate) async fn edit_poll_page(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
) -> ApiResult<status::Custom<Template>> {
    let poll = conn.get_poll(poll_id.clone()).await?;

    pick_response(
        conn.validate_poll_admin(poll_id, admin_id).await,
        "edit/admin-specific",
        &poll,
    )
}

/// Returns a page which displays a [`Poll`]'s results.
#[openapi(tag = "Admin pages")]
#[get("/<poll_id>/results/<admin_id>", rank = 3)]
pub(crate) async fn results_page(
    conn: DbConn,
    poll_id: String,
    admin_id: String,
) -> ApiResult<status::Custom<Template>> {
    let poll = conn.get_poll(poll_id.clone()).await?;

    pick_response(
        conn.validate_poll_admin(poll_id, admin_id).await,
        "results/admin-specific",
        &poll,
    )
}

// Vote-pages

/// Returns a page which lets visitors vote on the [`Poll`] corresponding to the given `poll_id`.
#[openapi(tag = "Vote pages")]
#[get("/<poll_id>", rank = 3)]
pub(crate) async fn vote(conn: DbConn, poll_id: String) -> ApiResult<Template> {
    let poll = conn.get_poll(poll_id.clone()).await?;

    Ok(Template::render("vote", &poll))
}

/// Returns a page which displays paths to actions the voter might want to take.
#[openapi(tag = "Vote pages")]
#[get("/<poll_id>/vote-links/<vote_id>", rank = 3)]
pub(crate) async fn vote_links_page(
    conn: DbConn,
    poll_id: String,
    vote_id: String,
) -> ApiResult<status::Custom<Template>> {
    let poll = conn.get_poll(poll_id.clone()).await?;

    pick_response(
        conn.validate_vote_id(poll_id, vote_id).await,
        "vote-links/vote-specific",
        &poll,
    )
}

/// Returns a page which allows for changing a [`Vote`]'s and the corresponding [`User`]'s information.
#[openapi(tag = "Vote pages")]
#[get("/<poll_id>/update/<vote_id>", rank = 3)]
pub(crate) async fn update_vote_page(
    conn: DbConn,
    poll_id: String,
    vote_id: String,
) -> ApiResult<status::Custom<Template>> {
    let poll = conn.get_poll(poll_id.clone()).await?;

    pick_response(
        conn.validate_vote_id(poll_id, vote_id).await,
        "update/vote-specific",
        &poll,
    )
}
