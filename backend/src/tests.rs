#[cfg(test)]
use crate::api;
use crate::models::{
    db_models::Poll,
    helper_models::{BareUserVote, PollResults, SetupData},
};
use crate::rocket;
use chrono::{Duration, SubsecRound, Utc};
use rocket::{http::Status, local::blocking::Client};

#[test]
fn poll_life_cycle() {
    let client = Client::tracked(rocket()).expect("valid rocket instance");

    // Create initial, "client-side" values.
    let mut initial_poll = Poll {
        poll_id: "".to_string(),
        title: "Test Poll".to_string(),
        instruction: "Test a poll. These are the instructions.".to_string(),
        datetime_interval: 60,
        offered_datetimes: Vec::from([
            (Utc::now() + Duration::seconds(305)).round_subsecs(0),
            (Utc::now() + Duration::seconds(9394)).round_subsecs(0),
            (Utc::now() + Duration::days(4)).round_subsecs(0),
        ]),
        //deadline: (Utc::now() + Duration::weeks(52)).round_subsecs(0),
        theme: "octopus".to_string(),
    };

    let initial_setup_data = SetupData::from_poll(
        &initial_poll, /* , &Some("me@testmail.com".to_string()) */
    );

    // Test creation of poll
    let response = client
        .post(uri!(api::create_poll))
        .json(&initial_setup_data)
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`create_poll()` did not return `Status::Ok`."
    );
    let next_page: String = response.into_string().unwrap();
    assert_eq!(
        client.get(&next_page).dispatch().status(),
        Status::Ok,
        "`create_poll()` returned the url to an invalid next page."
    );

    // Extract returned information
    let segments: Vec<&str> = next_page.split('/').collect();
    let poll_id: &str = segments[segments.len() - 3];
    let admin_id: &str = segments[segments.len() - 1];
    initial_poll.poll_id = poll_id.to_string();
    //let admin_action = |page: &str| -> String { format!("{poll_id}/{page}/{admin_id}") };

    // Test whether the created [`SetupData`] is the same as the one sent to the server.
    let response = client
        .get(uri!(api::get_poll_setupdata(poll_id, admin_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_poll_setupdata()` did not return `Status::Ok`."
    );
    let db_setup_data: SetupData = response.into_json().unwrap();
    assert_eq!(
        initial_setup_data, db_setup_data,
        "Returned SetupData does not match initial SetupData"
    );

    // Test whether the created [`Poll`] is the same as the one sent to the server.
    let response = client.get(uri!(api::get_poll_data(poll_id))).dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_poll_data()` did not return `Status::Ok`."
    );
    let db_poll: Poll = response.into_json().unwrap();
    assert_eq!(
        initial_poll, db_poll,
        "Returned Poll does not match initial Poll"
    );

    // Test receiving results
    let response = client
        .get(uri!(api::get_results_data(poll_id, admin_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_results_data()` did not return `Status::Ok`."
    );
    let db_results: PollResults = response.into_json().unwrap();
    assert_eq!(
        initial_poll, db_results.poll,
        "Returned Poll (from PollResults) does not match initial Poll"
    );

    // Test editing Poll (SetupData, actually)
    let mut edited_setup_data = initial_setup_data.clone();
    edited_setup_data.title = "Edited Title".to_string();
    assert_ne!(
        initial_setup_data, edited_setup_data,
        "Manuel editing of SetupData did not work."
    );
    let response = client
        .patch(uri!(api::edit_poll(poll_id, admin_id)))
        .json(&edited_setup_data)
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`edit_poll()` did not return `Status::Ok`."
    );
    assert_eq!(
        client
            .get(response.into_string().unwrap())
            .dispatch()
            .status(),
        Status::Ok,
        "`edit_poll()` returned the url to an invalid next page."
    );

    // Test whether the server's edited [`SetupData`] is the same as the edited client-one.
    let response = client
        .get(uri!(api::get_poll_setupdata(poll_id, admin_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_poll_setupdata()` did not return `Status::Ok`."
    );
    let db_setup_data: SetupData = response.into_json().unwrap();
    assert_eq!(
        edited_setup_data, db_setup_data,
        "Returned SetupData does not match edited SetupData"
    );
    assert_ne!(
        initial_setup_data, db_setup_data,
        "Returned SetupData does matches initial SetupData -> Editing did not work"
    );

    // Test whether the server's edited [`Poll`] is different from the initial client-one.
    let response = client.get(uri!(api::get_poll_data(poll_id))).dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_poll_data()` did not return `Status::Ok`."
    );
    let db_poll: Poll = response.into_json().unwrap();
    assert_ne!(
        initial_poll, db_poll,
        "Returned Poll matches initial Poll -> Editing did not work"
    );

    // Test deletion of Poll
    let response = client
        .delete(uri!(api::delete_poll(poll_id, admin_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`delete_poll()` did not return `Status::Ok`."
    );
    assert_eq!(
        client
            .get(response.into_string().unwrap())
            .dispatch()
            .status(),
        Status::Ok,
        "`delete_poll()` returned the url to an invalid next page."
    );
    // Check that poll actually got deleted.
    assert_ne!(
        client
            .get(uri!(api::get_poll_data(poll_id)))
            .dispatch()
            .status(),
        Status::Ok,
        "`get_poll_data()` had `Status::Ok`, even though it should not have found a poll."
    );
    assert_ne!(
        client
            .get(uri!(api::get_poll_setupdata(poll_id, admin_id)))
            .dispatch()
            .status(),
        Status::Ok,
        "`get_poll_setupdata()` had `Status::Ok`, even though it should not have found a poll."
    );
}

#[test]
fn vote_life_cycle() {
    let client = Client::tracked(rocket()).expect("valid rocket instance");

    // Create initial, "client-side" values.
    let initial_poll = Poll {
        poll_id: "".to_string(),
        title: "Test Poll".to_string(),
        instruction: "Test a poll. These are the instructions.".to_string(),
        datetime_interval: 60,
        offered_datetimes: Vec::from([
            (Utc::now() + Duration::seconds(305)).round_subsecs(0),
            (Utc::now() + Duration::seconds(9394)).round_subsecs(0),
            (Utc::now() + Duration::days(4)).round_subsecs(0),
        ]),
        //deadline: (Utc::now() + Duration::weeks(52)).round_subsecs(0),
        theme: "octopus".to_string(),
    };
    let initial_setup_data = SetupData::from_poll(
        &initial_poll, /* , &Some("me@testmail.com".to_string()) */
    );
    let initial_uservote_1 = BareUserVote {
        name: String::from("Max Mustermann"),
        comment: Some(String::from("Hi, dear tester! :)")),
        availability: Vec::from([
            (Utc::now() + Duration::seconds(305)).round_subsecs(0),
            (Utc::now() + Duration::seconds(9394)).round_subsecs(0),
            (Utc::now() + Duration::days(4)).round_subsecs(0),
        ]),
        admin_edited: false,
    };
    let initial_uservote_2 = BareUserVote {
        name: String::from("XxHunter22xX"),
        comment: None,
        availability: Vec::new(),
        admin_edited: false,
    };

    // Create poll
    let response = client
        .post(uri!(api::create_poll))
        .json(&initial_setup_data)
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`create_poll()` did not return `Status::Ok`."
    );

    // Extract returned information
    let next_page: String = response.into_string().unwrap();
    let segments: Vec<&str> = next_page.split('/').collect();
    // Get IDs
    let poll_id: &str = segments[segments.len() - 3];
    let admin_id: &str = segments[segments.len() - 1];

    // Test receiving results
    let response = client
        .get(uri!(api::get_results_data(poll_id, admin_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_results_data()` did not return `Status::Ok`."
    );
    let db_results: PollResults = response.into_json().unwrap();
    assert!(
        db_results.uservotes.is_empty(),
        "BareUserVotes were not empty, even though no votes have been added yet. BareUserVotes: {:?}",
        db_results.uservotes,
    );

    // Test adding a vote
    let response = client
        .post(uri!(api::add_vote(poll_id)))
        .json(&initial_uservote_1)
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`add_vote()` did not return `Status::Ok`."
    );
    let next_page: String = response.into_string().unwrap();
    assert_eq!(
        client.get(&next_page).dispatch().status(),
        Status::Ok,
        "`add_vote()` returned the url to an invalid next page."
    );

    // Extract returned information
    let segments: Vec<&str> = next_page.split('/').collect();
    // Get IDs
    let vote_poll_id: &str = segments[segments.len() - 3];
    let vote_1_id: &str = segments[segments.len() - 1];
    assert_eq!(vote_poll_id, poll_id);

    // Test receiving vote-data
    let response = client
        .get(uri!(api::get_vote_data(poll_id, vote_1_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_results_data()` did not return `Status::Ok`."
    );
    let db_vote: BareUserVote = response.into_json().unwrap();
    assert_eq!(
        db_vote, initial_uservote_1,
        "Returned BareUserVote did not match initial BareUserVote."
    );

    // Test adding a second vote
    let response = client
        .post(uri!(api::add_vote(poll_id)))
        .json(&initial_uservote_2)
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`add_vote()` did not return `Status::Ok`."
    );
    assert_eq!(
        client
            .get(response.into_string().unwrap())
            .dispatch()
            .status(),
        Status::Ok,
        "`add_vote()` returned the url to an invalid next page."
    );

    // Test receiving results
    let response = client
        .get(uri!(api::get_results_data(poll_id, admin_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_results_data()` did not return `Status::Ok`."
    );
    let db_results: PollResults = response.into_json().unwrap();
    assert_eq!(
        db_results.uservotes.len(),
        2,
        "There were not exactly two BareUserVotes. BareUserVotes: {:?}",
        db_results.uservotes,
    );
    assert!(
        db_results.contains_bareuservote(&initial_uservote_1),
        "Returned BareUserVote did not match initial BareUserVote_1."
    );
    assert!(
        db_results.contains_bareuservote(&initial_uservote_2),
        "Returned BareUserVote did not match initial BareUserVote_2."
    );

    // Test editing (updating) a vote
    let mut updated_uservote_1 = initial_uservote_1.clone();
    updated_uservote_1.comment = None;
    updated_uservote_1.admin_edited = true;
    let response = client
        .patch(uri!(api::update_vote(poll_id, vote_1_id)))
        .json(&updated_uservote_1)
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`update_vote()` did not return `Status::Ok`."
    );
    assert_eq!(
        client
            .get(response.into_string().unwrap())
            .dispatch()
            .status(),
        Status::Ok,
        "`update_vote()` returned the url to an invalid next page."
    );
    // Check whether the updated vote actually has changed.
    let response = client
        .get(uri!(api::get_vote_data(poll_id, vote_1_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_vote_data()` did not return `Status::Ok`."
    );
    let db_updated_vote: BareUserVote = response.into_json().unwrap();
    assert_eq!(
        db_updated_vote, updated_uservote_1,
        "Returned (updated) BareUserVote did not match updated BareUserVote."
    );
    assert_ne!(
        db_updated_vote, initial_uservote_1,
        "Returned (updated) BareUserVote still matches initial BareUserVote."
    );

    // Test the `unset_admin_edited` function
    let response = client
        .patch(uri!(api::unset_admin_edited(poll_id, vote_1_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`unset_admin_edited()` did not return `Status::Ok`."
    );
    // Check whether the vote's `admin_edited` field actually has changed.
    let response = client
        .get(uri!(api::get_vote_data(poll_id, vote_1_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`get_vote_data()` did not return `Status::Ok`."
    );
    let db_updated_vote_v2: BareUserVote = response.into_json().unwrap();
    assert_eq!(
        db_updated_vote_v2.admin_edited, false,
        "Returned (updated) BareUserVote did not match updated BareUserVote."
    );
    assert_ne!(
        db_updated_vote.admin_edited, db_updated_vote_v2.admin_edited,
        "Returned (updated) BareUserVote still matches initial BareUserVote."
    );

    // Test deleting a vote
    let response = client
        .delete(uri!(api::delete_vote(poll_id, vote_1_id)))
        .dispatch();
    assert_eq!(
        response.status(),
        Status::Ok,
        "`delete_vote()` did not return `Status::Ok`."
    );
    assert_eq!(
        client
            .get(response.into_string().unwrap())
            .dispatch()
            .status(),
        Status::Ok,
        "`delete_vote()` returned the url to an invalid next page."
    );

    // Check that the vote actually got deleted.
    assert_ne!(
        client
            .get(uri!(api::get_vote_data(poll_id, vote_1_id)))
            .dispatch()
            .status(),
        Status::Ok,
        "`get_vote_data()` had `Status::Ok`, even though it should not have found a poll."
    );

    // Delete Poll
    let _ = client
        .delete(uri!(api::delete_poll(poll_id, admin_id)))
        .dispatch();
}
