//! Defines the catchers for all possible error-codes.

use crate::api::file_server_api::ROOT_ROUTE;
use rocket::{fs::NamedFile, response::Redirect, Request};
use std::{io, path::PathBuf};

/// Counts the segments of a request.
fn segment_count(req: &Request<'_>) -> usize {
    req.segments::<PathBuf>(0..)
        .unwrap()
        .to_str()
        .unwrap()
        .matches('/')
        .count()
}

/// Displays some kind of "This Poll does not exist" message in case of most errors.
#[catch(default)]
async fn default() -> Redirect {
    Redirect::to(uri!("/404.html"))
}
#[catch(404)]
async fn not_found(req: &Request<'_>) -> io::Result<NamedFile> {
    NamedFile::open(format!(
        "{ROOT_ROUTE}{}/404.html",
        match segment_count(req) {
            2 => "/slash/slash",
            1 => "/slash",
            _ => "",
        }
    ))
    .await
}
/// Used when there is some internal error.
#[catch(500)]
async fn internal(req: &Request<'_>) -> io::Result<NamedFile> {
    NamedFile::open(format!(
        "{ROOT_ROUTE}{}/500.html",
        match segment_count(req) {
            2 => "/slash/slash",
            1 => "/slash",
            _ => "",
        }
    ))
    .await
}

/// Returns all catchers in [self].
pub fn catchers() -> Vec<rocket::Catcher> {
    catchers![default, not_found, /* unprocessable, */ internal]
}
