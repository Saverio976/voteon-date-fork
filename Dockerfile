FROM rust:latest

# Install node
#https://github.com/nodesource/distributions
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash - \
    && apt-get install -y nodejs

#ARG DATABASE_URL
ARG ROCKET_DATABASES

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY . .

# Install deps
RUN cd frontend && npm install && npm run build && cd build && ls
RUN cd backend && cargo build --release

# Start
CMD ["bash", "-c", "cd backend && target/release/voteon_date"]